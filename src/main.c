#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <crypt.h>

#define LENGTH(str) ((sizeof(str) / sizeof(str[0])))

struct data
{
    int length;
    char wow,  alg, *key;
};

static const char abc[] = 
        "0123456789"
        "qwertyuiopasdfghjklzxcvbnm"
        "QWERTYUIOPASDFGHJKLZXCVBNM";

static const char abc_wow[] = 
        "0123456789"
        "qwertyuiopasdfghjklzäxcvbnm"
        "QWERTYUIOPASDFGHJKLZÄXCVBNM"
        "!@#$%^&*()_+}{:\">?<;,./~`";

char *random_string(struct data *_data);
char *c_crypt(struct data *_info, char *plain_text);
void get_flags(int argc, char **argv, struct data *_info)
{
    static int verb_flag = 0;
    int c;
    while (1)
    {
        static struct option long_options[] = 
        {
            {"help", no_argument, 0, 'h'},
            //other symbols, example: "@", "!", "#", "&" and other.
            {"wow", no_argument, 0, 'w'},
            {"key", required_argument, 0, 'k'},
            {"crypt", no_argument, 0, 'c'},
            {"length", required_argument, 0, 'l'},
            {0,0,0,0}
        };
        int option_index = 0;
        c = getopt_long(argc, argv, "ha:l:wck:", long_options, &option_index);
        if (c == -1)
            break;
        
        switch (c)
        {
            case 'h':
                printf("USAGE:\n--key & -k\t key for hashing algorithm\n");
                printf("--crypt & -c\t standard C hashing algorithm\n");
                printf("--length & -l\t hash length\n");
                printf("--wow & -w\t use special symbols\n");
                printf("--help & -h\t help\n");
                exit(1);

            case 'l':
                _info->length = atoi(optarg);
                break;

            case 'w':
                printf("WOW!\n");
                _info->wow = 1;
                break;
            
            case 'c':
                _info->alg = 1;
                break;

            case 'k':
                _info->key = optarg;
                break;

            case '?':
                exit(-1);

            default:
                break;
        }
    }
}

int main(int argc, char **argv)
{
    struct data _info = {100, 0, 0, 0};
    get_flags(argc, argv, &_info);
    if (_info.alg == 0)
    {
        printf("%s\n", random_string(&_info));
    }
    if (_info.alg == 1)
    {
        printf("%s\n", c_crypt(&_info,random_string(&_info)));
    }
    return 0;
}
 
char *random_string(struct data *_info)
{
    srand(time(NULL) * getpid());
    char *str = (char *)malloc(_info->length++);
    if (str == NULL)
        exit(-1);

    for (int i = 0; i < _info->length; i++)
    {
        str[i] = _info->wow == 1 ? abc_wow[rand() % LENGTH(abc_wow)] : abc[rand() % LENGTH(abc)];
    }
    return str;
}

char *c_crypt(struct data *_info, char *plain_text)
{
    srand(time(NULL) * getpid());
    char all[40323];
    snprintf(all, sizeof all, "%s%s%s%s", "$6", "$WOW!", "$", plain_text);
    return crypt(_info->key == (char *)1 ? _info->key : plain_text, all);
}
